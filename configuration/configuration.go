package configuration

import (
	"io/ioutil"
	"log"
	"os"

	"github.com/BurntSushi/toml"
)

// Config contains all the objects defined in config.toml
type Config struct {
	AerospikeHost     string
	AerospikePort     int
	ArtistID          string
	AerospikeAuthDB   string
	TLS               bool
	RequestURL        string
	AppID             string
	Format            string
	EventDB           string
	RedisPW           string
	RedisPort         int
	PubNubChannel     string
	Subscription      string
	Subscriptions     string
	Delete            string
	Hello             string
	Channel           string
	Subscribe_key     string
	Publish_key       string
	Unsubscription    string
	NotifRBackend     string
	NotifRPostBackend string
	HostBackend       string
}

// ReadConfig Reads info from config file
func ReadConfig() (Config, error) {
	env := os.Getenv("ENV")
	gopath := os.Getenv("GOPATH")
	backendhost := os.Getenv("HOST-BACKEND")
	path := gopath + "/src/zhaw.ch/microservice.bands/configuration/config.toml"
	configfile, err := ioutil.ReadFile(path)
	if err != nil {
		log.Println(env+" is environment. Config file is missing: ", path)
		return Config{}, err
	}

	var config Config
	if _, err := toml.Decode(string(configfile), &config); err != nil {
		log.Println("Config could not be decoded: ", err)
		return Config{}, err
	}
	if env != "" {
		config.AerospikeHost = config.AerospikeHost + env
		if env == "test" {
			config.AerospikePort = 3004
		}
	} else {
		config.AerospikeHost = "127.0.0.1"
	}
	if backendhost != "" {
		config.HostBackend = "http://" + backendhost
	}
	return config, nil
}
