package configuration

import (
	"os"
	"testing"
)

func Test_ReadConfig_Pass(t *testing.T) {
	os.Setenv("ENV", "Test")
	config, err := ReadConfig()
	if err != nil {
		t.Fatalf("Error in read config: %v", err)
	}
	if config.AppID != "notifR" {
		t.Fatalf("Unable to read the Config or the AppID is wrong!")
	}
}

func Test_ReadConfig_Fail(t *testing.T) {
	config, err := ReadConfig()
	if err != nil {
		t.Fatalf("There shouldn't be an error: %v", config)
	}
}
