package stringUtility

import (
	"log"

	"zhaw.ch/microservice.bands/configuration"
)

//GenerateString for get Request
func GenerateString(actor string) string {
	config, err := configuration.ReadConfig()
	if err != nil {
		log.Fatal("reading Configuration failed ", err)
	}
	baseURL := config.RequestURL
	appID := config.AppID
	format := config.Format
	return baseURL + actor + "/events?app_id=" + appID + format
}
