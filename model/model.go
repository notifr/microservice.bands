package model

import (
	"time"

	as "github.com/aerospike/aerospike-client-go"
)

//Event defined as Strict
type Event struct {
	ID       string   `json:"id"`
	ArtistID string   `json:"artist_id"`
	URL      string   `json:"url"`
	Title    string   `json:"title"`
	Datetime string   `json:"datetime"`
	Lineup   []string `json:"lineup"`
	Venue    Venue    `json:"venue"`
}

//Venue Defined as struct
type Venue struct {
	Name      string `json:"name"`
	City      string `json:"city"`
	Region    string `json:"region"`
	Country   string `json:"country"`
	Latitude  string `json:"latitude"`
	Longitude string `json:"longitude"`
}

// DateTime saves date in struct
type DateTime struct {
	time.Time
}

//Offers defined as struct
type Offers struct {
	Type   string `json:"type"`
	URL    string `json:"url"`
	Status string `json:"status"`
}

//Controller object as a mgo Session
type Controller struct {
	Session as.Client
}

//Message type used to receive and publish
type Message struct {
	Type string `json:"message"`
}

// Subscription definition
type Subscription struct {
	ID      string `json:"id"`
	SubInfo Data   `json:"data"`
}

// Data containing SubscriptionInfo
type Data struct {
	Band    string `json:"band"`
	Country string `json:"country"`
}

// Subscriptions are an array of Subscription
type Subscriptions struct {
	Subscription []Subscription `json:"subscriptions"`
}

// SubscriptionData contains the data Block of the subscriptions
type SubscriptionData struct {
	SubscriptionData Subscriptions `json:"data"`
}

// Connector holds information about the connector
type Connector struct {
	Connector string `json:"connector"`
}

// Startup Message
type Startup struct {
	Message   string    `json:"message"`
	Connector Connector `json:"data"`
}

// Unsubscribe is a type struct ob unsubscribe
type Unsubscribe struct {
	Unsubscribe UnSubscriptionData `json:"data"`
}

//UnSubscriptionData contains an Array of UnSubscriptions
type UnSubscriptionData struct {
	UnSubscriptions []UnSubscription `json:"subscriptions"`
}

// UnSubscription is the ID that has to be unsubscribed
type UnSubscription struct {
	ID string `json:"id"`
}

//Notification which contains the message "notify"
//and the NotificationData
type Notification struct {
	Message string           `json:"message"`
	Data    NotificationData `json:"data"`
}

// NotificationData contains the message and
//the NotifySubscriptions which have to be notified
type NotificationData struct {
	NotificationMessage string               `json:"notificationMessage"`
	NotifySubscriptions []NotifySubscription `json:"subscriptions"`
}

// NotifySubscription that has to be notified
type NotifySubscription struct {
	ID string `json:"id"`
}

// SubscriptionToNotify (used for new Subscriptions)
type SubscriptionToNotify struct {
	IsNew          bool
	SubscriptionID string
	Country        string
}
