package main

import (
	"os"
	"testing"

	"zhaw.ch/microservice.bands/model"
	"zhaw.ch/microservice.bands/persistenceUtil"
)

func insertTestdata() {
	var sub model.Subscription
	sub.ID = "123"
	sub.SubInfo.Band = "solstafir"
	sub.SubInfo.Country = "CH"
	persistenceUtil.StoreSubscription(sub)
}

func Test_PushAndPull_Pass(t *testing.T) {
	periodicPushAndPull()
}

func TestMain(m *testing.M) {
	insertTestdata()
	result := m.Run()
	os.Exit(result)
}
