package persistenceUtil

import (
	"log"

	"fmt"

	"zhaw.ch/microservice.bands/model"
)

var subs model.Subscriptions

func appendSubscription(sub model.Subscription) bool {
	log.Println("received following data to store: " + sub.SubInfo.Band + " " + sub.ID + " " + sub.SubInfo.Country)
	if containsSubscription(sub) {
		fmt.Println("subscription already exist in subs")
		return false
	}
	if sub.SubInfo.Band != "" && sub.SubInfo.Country != "" && sub.ID != "" {

		subs.Subscription = append(subs.Subscription, sub)
		return containsSubscription(sub)
	}
	return false
}

func containsSubscription(subscription model.Subscription) bool {
	for _, sub := range subs.Subscription {
		if sub.ID == subscription.ID {
			return true
		}
	}
	return false
}

// GetSubscriptions as array of Subscription
func GetSubscriptions() []model.Subscription {
	return subs.Subscription

}

// RemoveSubscription removes a given Unsubscription from subscription array (currently not implemented properly but it works)
func RemoveSubscription(unsub string) bool {

	for i := len(subs.Subscription) - 1; i >= 0; i-- {
		sub := subs.Subscription[i]
		// Condition to decide if current element has to be deleted:
		if sub.ID == unsub {
			subs.Subscription = append(subs.Subscription[:i], subs.Subscription[i+1:]...)
			log.Println("Content of subs after deleting: ", subs)
			return true
		}
	}
	return false
}
