package persistenceUtil

import (
	"strconv"
	"testing"
	"time"

	"zhaw.ch/microservice.bands/model"
)

func Test_ReadConfig_Fail(t *testing.T) {
	var sub model.Subscription
	err := StoreSubscription(sub)
	if err == nil {
		t.Fatalf("should have an error: %v", err)
	} else {
		t.Log("succes, able to handle empty sub object")
	}
}

func Test_StoreArtist_Success(t *testing.T) {
	var sub model.Subscription
	sub.ID = "1235"
	sub.SubInfo.Band = "Kreator"
	sub.SubInfo.Country = "CH"
	err := StoreSubscription(sub)
	if err != nil {
		t.Fatalf("shouldn't have an error: %v", err)
	} else {
		t.Log("succes, able to handle a sub object")
	}
}

func Test_getIDFromRequest_Success(t *testing.T) {
	actor := "Vader"
	id, err := getIDFromRequest(actor)
	if err != nil {
		t.Fatalf("shouldn't have an error: %v", err)
	} else {
		t.Log("succes, able to get id for " + actor + " it's " + id)
	}
}

func Test_GetBandName_Success(t *testing.T) {
	id := "34985"
	artist, err := GetBandName(id)
	if err != nil {
		t.Fatalf("shouldn't have an error: %v", err)
	} else {
		t.Log("succes, able to get name for " + id + " it's " + artist)
	}
}

func Test_ReadConfig_Success(t *testing.T) {
	var sub model.Subscription
	timestamp := strconv.FormatInt(time.Now().UTC().UnixNano(), 10)
	sub.ID = timestamp
	sub.SubInfo.Band = "Kreator"
	sub.SubInfo.Country = "CH"
	success := appendSubscription(sub)
	if !success {
		t.Fatalf("shouldn't have an error")
	} else {
		t.Log("succes, able to handle a sub object")
	}
}
