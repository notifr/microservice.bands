package persistenceUtil

import (
	"log"

	"zhaw.ch/microservice.bands/aerospike"
	"zhaw.ch/microservice.bands/configuration"
	"zhaw.ch/microservice.bands/model"
	"zhaw.ch/microservice.bands/requestHandler"
	"zhaw.ch/microservice.bands/stringUtility"
)

//StoreSubscription in persistence
func StoreSubscription(sub model.Subscription) error {
	appendSubscription(sub)
	artist := sub.SubInfo.Band

	artistID, err := getIDFromRequest(artist)
	if err != nil || artistID == "" {
		log.Println("failed to get ID for requested Band "+artist, err)
		return err
	}
	exist, err := existInDB(artistID, "bands")
	if !exist && err == nil {
		log.Println("store band " + artist + " with id " + artistID + " in db.")
		return storeData(artistID, artist, "bands")
	}
	return err
}

func storeData(key string, value string, namespace string) error {
	return aerospike.InsertData(key, value, namespace)
}

// GetData from db
func GetData() ([]string, error) {
	//bandMap := GetSubscriptions()
	bandMap, err := aerospike.GetValues("bands")
	if err != nil {
		var bandMapErr []string
		return bandMapErr, err
	}
	bands := make([]string, 0, len(bandMap))

	for _, value := range bandMap {
		log.Println(value + " read from the store")
		bands = append(bands, value)
	}
	return bands, nil
}

func getIDFromRequest(actor string) (string, error) {
	events, err := requestHandler.SendRequest(stringUtility.GenerateString(actor))
	if err != nil || len(events) < 2 {
		log.Printf("failed to receive an answer for "+actor+" error message: ", err)
		return "", err
	}
	ID := events[0].ArtistID
	return ID, nil
}

// GetBandName from a given ID
func GetBandName(id string) (string, error) {
	bandMap, err := aerospike.GetValues("bands")
	return bandMap[id], err
}

// StoreEventInDB if it not exists
func StoreEventInDB(event model.Event) (bool, error) {
	config, err := configuration.ReadConfig()
	exist, err := existInDB(event.ID, config.EventDB)
	if !exist {
		err = aerospike.InsertData(event.ID, event.ArtistID, config.EventDB)
		if err != nil {
			log.Println("there was an error inserting the record! "+config.EventDB, err)
		}
	}
	return exist, err

}

func existInDB(key string, namespace string) (bool, error) {
	return aerospike.ExistInDB(key, namespace)
}

// DeleteSubscription from persistence
func DeleteSubscription(sub string) bool {
	return RemoveSubscription(sub)
}
