package requestHandler

import (
	"testing"
)

func TestHttpHandler_SendRequest_Fail(t *testing.T) {
	response, err := SendRequest("https://rest.bandsintown.com/artists/blabla")
	if err == nil {
		t.Fail()
	}
	if response != nil {
		t.Log(response)
	}
}

func TestHttpHandler_SendRequest_Pass(t *testing.T) {
	response, err := SendRequest("https://rest.bandsintown.com/artists/kreator/events?app_id=notifR")
	if err != nil {
		t.Fail()
	}
	if response == nil {
	}
}

func TestHttpHandler_HandleRequests_Pass(t *testing.T) {
	requests := make([]string, 1)
	requests[0] = "https://rest.bandsintown.com/artists/eisregen/events?app_id=notifR"
	response, err := HandleRequests(requests)
	if err != nil {
		t.Fail()
	}
	if response == nil {
	}

}
