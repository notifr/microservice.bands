package requestHandler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"zhaw.ch/microservice.bands/model"
)

// SendRequest makes a GET-Request
func SendRequest(request string) ([]model.Event, error) {
	events := make([]model.Event, 0)

	if len(request) < 2 {
		fmt.Println("failed to execute ", request)
		os.Exit(1)
	}
	response, err := http.Get(request)
	if err != nil {
		log.Println("getting Request failed", err)
		return nil, err
	}

	defer response.Body.Close()
	res, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println(err)
	} else {
		err = json.Unmarshal(res, &events)
		if err != nil {
			log.Println("parse JSON failed", err)
		}
	}

	return events, err
}

// HandleRequests and publish them to publisher
func HandleRequests(requests []string) ([]model.Event, error) {
	var eventCollection []model.Event
	var events []model.Event
	var err error
	for i := 0; i < len(requests); i++ {
		events, err = SendRequest(requests[i])
		if err != nil {
			log.Println("unable to get Answer from Request", err)
		} else {
			for _, element := range events {
				eventCollection = append(eventCollection, element)
			}
		}

	}
	return eventCollection, err
}
