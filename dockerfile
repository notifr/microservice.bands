FROM golang:latest
RUN mkdir /app
ARG ENV
ARG BACKEND-HOST

ADD src/zhaw.ch/microservice.bands/main /app/

RUN mkdir -p /go/src/zhaw.ch/microservice.bands/configuration
ADD src/zhaw.ch/microservice.bands/configuration/config.toml /go/src/zhaw.ch/microservice.bands/configuration
ENV ENV=$ENV
ENV BACKEND-HOST=$BACKEND-HOST
WORKDIR /app
CMD ["/app/main"]
