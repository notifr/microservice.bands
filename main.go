package main

import (
	"log"

	"github.com/jasonlvhit/gocron"

	"zhaw.ch/microservice.bands/model"
	"zhaw.ch/microservice.bands/publisher"
	"zhaw.ch/microservice.bands/receiver"
	"zhaw.ch/microservice.bands/requestBuilder"
	"zhaw.ch/microservice.bands/requestHandler"
)

func main() {
	go func() { receiver.Receive() }()
	//persistenceUtil.StoreArtist("Slayer")
	gocron.Every(1).Minutes().Do(periodicPushAndPull)
	<-gocron.Start()
	receiver.StopReceive()
	log.Printf("stopped Process")
}

func periodicPushAndPull() {
	requests, err := requestBuilder.BuildRequest()
	if err != nil {
		log.Println(requests, err)
	}
	events, err := requestHandler.HandleRequests(requests)
	if err != nil {
		log.Println("handling requests failed", err)
	}
	var subscriptionToNotivy model.SubscriptionToNotify
	subscriptionToNotivy.IsNew = false
	err = publisher.PublishEvents(events, subscriptionToNotivy)
	if err != nil {
		log.Println("publishing events failed", err)
	}
}
