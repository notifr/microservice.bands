package pubnub

import (
	"encoding/json"
	"fmt"

	"zhaw.ch/microservice.bands/configuration"
	"zhaw.ch/microservice.bands/model"
	"zhaw.ch/microservice.bands/persistenceUtil"
	"zhaw.ch/microservice.bands/publisher"
	"zhaw.ch/microservice.bands/requestHandler"
	"zhaw.ch/microservice.bands/stringUtility"
)

//HandleMessage received from Pubnub Store/Publish (Subscriptions)
func HandleMessage(message string) error {
	msg := []byte(message)
	config, err := configuration.ReadConfig()
	if err != nil {
		fmt.Println("not possible to unmarshal stream", err)
		return err
	}
	var msgType model.Message
	err = UnmarshalSubscription(msg, &msgType)
	fmt.Println(msgType.Type)
	if msgType.Type == config.Subscription || msgType.Type == config.Subscriptions {
		var data model.SubscriptionData
		err = UnmarshalSubscription(msg, &data)
		fmt.Print(data.SubscriptionData.Subscription)
		for _, sub := range data.SubscriptionData.Subscription {
			saveSubscription(sub)
		}
	}
	if msgType.Type == config.Unsubscription {
		var data model.Unsubscribe
		fmt.Println("deleting " + message)
		err = UnmarshalSubscription(msg, &data)
		for _, unsubscription := range data.Unsubscribe.UnSubscriptions {
			fmt.Println(unsubscription.ID + " is")
			if !persistenceUtil.DeleteSubscription(unsubscription.ID) {
				fmt.Println("error deleting subscription")
			}
		}

	}
	return err
}

// UnmarshalSubscription from pubnub
func UnmarshalSubscription(msg []byte, v interface{}) error {
	err := json.Unmarshal(msg, v)
	if err != nil {
		fmt.Println("not possible to unmarshal stream", msg, err)
		return err
	}
	return nil
}

func saveSubscription(sub model.Subscription) {
	fmt.Println("save Subscription " + sub.ID + "for Band " + sub.SubInfo.Band)
	persistenceUtil.StoreSubscription(sub)
	requestString := stringUtility.GenerateString(sub.SubInfo.Band)
	events, err := requestHandler.SendRequest(requestString)
	if err == nil {
		var subscriptionToNotivy model.SubscriptionToNotify
		subscriptionToNotivy.IsNew = true
		subscriptionToNotivy.SubscriptionID = sub.ID
		subscriptionToNotivy.Country = sub.SubInfo.Country
		publisher.PublishEvents(events, subscriptionToNotivy)
	} else {
		fmt.Println("error getting events from request!")
	}
}
