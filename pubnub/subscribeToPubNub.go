package pubnub

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/pubnub/go/messaging"
	"zhaw.ch/microservice.bands/configuration"
)

var running bool

//SubscribeToPubnub channel
func SubscribeToPubnub() error {
	config, err := configuration.ReadConfig()
	if err != nil {
		log.Println("not able to read Configuration", err)
	}
	go func() { sayHello(config) }()
	running = true
	successChannel := make(chan []byte)
	errorChannel := make(chan []byte)
	pubnub := messaging.NewPubnub(config.Publish_key, config.Subscribe_key, "", "", false, "", nil)
	go pubnub.Subscribe(config.Channel, "", successChannel, false, errorChannel)
	for running {
		select {
		case response := <-successChannel:
			var msg []interface{}
			err := json.Unmarshal(response, &msg)
			if err != nil {
				fmt.Println(err)
			}
			switch m := msg[0].(type) {
			case float64:
				fmt.Println(msg[1].(string))
			case []interface{}:
				fmt.Printf("Received message '%s' on channel '%s'\n ", m[0], msg[2])
				message := fmt.Sprint(m[0])
				HandleMessage(message)
			default:
				panic(fmt.Sprintf("Unknown type: %T", m))
			}
		case err := <-errorChannel:
			fmt.Println(string(err))
		case <-messaging.SubscribeTimeout():
			fmt.Println("Subscribe() timeout")
		}
	}
	return err
}

// StopSubscription by changing running to false
func StopSubscription() {
	running = false
}

func sayHello(config configuration.Config) error {

	response, err := http.Get(config.HostBackend + config.NotifRBackend)
	if err != nil || response == nil {
		log.Println("There was an error in getting Request using "+config.NotifRBackend+"  ", err)
		return err
	}
	fmt.Println(response)
	defer response.Body.Close()
	res, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println(err)
	}
	HandleMessage(string(res))
	return err
}
