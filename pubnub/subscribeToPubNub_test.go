package pubnub

import (
	"testing"

	"zhaw.ch/microservice.bands/configuration"
)

func Test_sayHello_Fail(t *testing.T) {
	var config configuration.Config
	err := sayHello(config)
	if err == nil {
		t.Fatalf("should have an error: %v", err)
	} else {
		t.Log("succes, able to handle empty sub object")
	}
}
