# Dependencies

Golang v1.7.0 or later

to build make sure you have set the gopath and the goworkdir

# Docker

## Aerospike
docker stop aerospike
docker rm aerospike
docker run -v /notifR/asLogs:/var/log/aerospike/ -v /notifR/asData:/opt/aerospike/data -v $GOPATH/src/zhaw.ch/microservice.bands/aerospike:/opt/aerospike/etc --name aerospike -p 3000:3000 -p 3001:3001 -p 3002:3002 -p 3003:3003 aerospike/aerospike-server /usr/bin/asd --foreground --config-file /opt/aerospike/etc/aerospike.conf



# JSON microservice:Bands


## Subscrition/Subscriptions

```
{
    message: <string>
    data: {
        subscriptions: [
            {
                id: <integer>
                data: [
                    band: <string>
                    country: <string>
                ]
            }
        ]
    }
}
```


## unsubscribe

```

{
    message: <string>
    data: {
        subscriptions: [
            { 
                id: <integer>
            }
        ]
    }
}
```

## notify

```
{
    message: <string>
    data: {
        notificationMessage: <string>
        subscriptions: [
            { 
                id: <integer>
            }
        ]
    }
}
```

## startup

```
{
    message: <string>
    data: {
        connector: <string>
    }
}
```

