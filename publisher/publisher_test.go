package publisher

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"zhaw.ch/microservice.bands/model"
)

func Test_sendHTTPPost_Fail(t *testing.T) {

	err := sendHTTPPost([]byte("Here is a string...."))
	assert.Equal(t, nil, err)
}

// needs internetconnection
func Test_sendHTTPPost_Success(t *testing.T) {
	err := sendHTTPPost([]byte(`{"message":"notify","data":{"notificationMessage":"The weather in Winterthur is Clouds at 19.59°C max is 20°C min is
scriptions":[{"id":"590b962d21ff6200f60aba46"}]}}`))
	assert.Equal(t, nil, err)
}

func Test_sendHTTPThread_Success(t *testing.T) {
	err := sendHTTPPost([]byte(`{"message":"notify","data":{"notificationMessage":"The weather in Winterthur is Clouds at 19.59°C max is 20°C min is
scriptions":[{"id":"590b962d21ff6200f60aba46"}]}}`))
	assert.Equal(t, nil, err)
}

func Test_createNotification_Success(t *testing.T) {
	var event model.Event
	testMessage := "This is only for testing purpose"
	event.ID = "1234"
	event.ArtistID = "13454"
	event.Datetime = "123T4546"
	event.Lineup = make([]string, 1)
	event.Lineup[0] = "niemer"
	event.Venue.Name = "döt"
	event.Venue.City = "nöd z winti"
	var subToNotify model.SubscriptionToNotify

	res, err := createNotification(event, subToNotify)
	assert.Nil(t, err)
	assert.NotEqual(t, testMessage, res.Data.NotificationMessage)
}
