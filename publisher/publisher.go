package publisher

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"encoding/json"

	"fmt"

	"zhaw.ch/microservice.bands/configuration"
	"zhaw.ch/microservice.bands/model"
	"zhaw.ch/microservice.bands/persistenceUtil"
)

var config configuration.Config

//PublishEvents given to PublishService and store diff in db
func PublishEvents(events []model.Event, subToNotify model.SubscriptionToNotify) error {
	var err error
	publishing := false
	for _, event := range events {

		inDB, err := persistenceUtil.StoreEventInDB(event)
		if !inDB || subToNotify.IsNew {
			//solve with go channel
			notification, err := createNotification(event, subToNotify)
			if err != nil {
				fmt.Println("not able to create Notification", err)
			}
			notificationAsByteArray, err := json.Marshal(notification)
			if err != nil {
				fmt.Println("not able to marshal notification", err)
			} else if notification.Data.NotifySubscriptions != nil {
				if !publishing {
					publishing = true
				}
				go func() { sendHTTPThread(notificationAsByteArray) }()
			}
		}

		if err != nil {
			log.Println("Failed to save event", err)
		}
	}
	if !publishing {
		fmt.Println("there was noting to publish")
	}
	return err
}

func sendHTTPThread(notificationAsByteArray []byte) {
	err := sendHTTPPost(notificationAsByteArray)
	if err != nil {
		fmt.Println("not able to send http POST", err)
	}
}

func createNotification(event model.Event, subToNotify model.SubscriptionToNotify) (model.Notification, error) {
	var notification model.Notification
	notification.Message = "notify"
	bandName, err := persistenceUtil.GetBandName(event.ArtistID)
	if err != nil {
		log.Println("error getting Bandname")
		return notification, err
	}
	date := strings.Split(event.Datetime, "T")
	notification.Data.NotificationMessage = bandName + " spielen in  " + event.Venue.City + " in " + event.Venue.Name + " am " + date[0] + " um " + date[1] + " Uhr."
	if subToNotify.IsNew != true {
		for _, sub := range persistenceUtil.GetSubscriptions() {
			if strings.ToLower(sub.SubInfo.Band) == strings.ToLower(bandName) && strings.ToLower(sub.SubInfo.Country) == strings.ToLower(event.Venue.Country) {
				var notifySubscription model.NotifySubscription
				notifySubscription.ID = sub.ID
				notification.Data.NotifySubscriptions = append(notification.Data.NotifySubscriptions, notifySubscription)
			}
		}
	} else if strings.ToLower(subToNotify.Country) == strings.ToLower(event.Venue.Country) {
		var notifySubscription model.NotifySubscription
		notifySubscription.ID = subToNotify.SubscriptionID
		notification.Data.NotifySubscriptions = append(notification.Data.NotifySubscriptions, notifySubscription)
	}

	return notification, nil

}

func sendHTTPPost(jsonStr []byte) error {
	config, err := configuration.ReadConfig()
	if err != nil {
		fmt.Println("Error reading config: ", err)
	}
	req, err := http.NewRequest("POST", config.HostBackend+config.NotifRPostBackend, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Cookie", "PHPSESSID=84518ae1db8595eda8242e137b886c7e")
	fmt.Println("notification as json: " + string(jsonStr))
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error executing request: ", err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))

	return err
}
