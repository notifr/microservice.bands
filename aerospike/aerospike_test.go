package aerospike

import (
	"testing"
)

func Test_CreateNewClient_Success(t *testing.T) {
	clt, err := CreateNewClient()
	if clt == nil || err != nil {
		t.Fatalf("There should be a client: %v", clt)
	}
}
