package aerospike

import (
	"log"
	"time"

	"zhaw.ch/microservice.bands/configuration"

	as "github.com/aerospike/aerospike-client-go"
)

var config configuration.Config

// CreateNewClient to connect to aerospike
func CreateNewClient() (*as.Client, error) {
	config, err := configuration.ReadConfig()
	client, err := as.NewClient(config.AerospikeHost, config.AerospikePort)
	return client, err
}

// InsertData into aerospike db
func InsertData(key string, value string, namespace string) error {
	client, err := CreateNewClient()
	if err != nil {
		return err
	}
	log.Println("insert " + key)
	// Initialize policy.
	policy := as.NewWritePolicy(0, 0)
	policy.Timeout = 50 * time.Millisecond // 100 millisecond timeout.
	policy.SendKey = true
	asKey, _ := as.NewKey(namespace, namespace, key)
	bin := as.NewBin(key, value)
	err = client.PutBins(policy, asKey, bin)
	defer client.Close()

	return err
}

// ExistInDB checks if a given key in a given namespace exists. return bool and error
func ExistInDB(id string, namespace string) (bool, error) {
	client, err := CreateNewClient()
	if err != nil {
		return false, err
	}
	defer client.Close()
	basePolicy := as.NewPolicy()
	key, err := as.NewKey(namespace, namespace, id)
	if err != nil {
		log.Println("not able to create new key ", err)
	}
	exist, err := client.Exists(basePolicy, key)
	if exist && err == nil {
		return true, err
	}
	log.Print("record ID " + id + " not in DB")
	return false, err
}

// GetValues from db
func GetValues(namespace string) (map[string]string, error) {
	bandMap := make(map[string]string)
	client, err := CreateNewClient()
	if err != nil {
		return bandMap, err
	}
	defer client.Close()
	spolicy := as.NewScanPolicy()
	spolicy.ConcurrentNodes = true
	spolicy.Priority = as.LOW
	spolicy.IncludeBinData = true

	recs, err := client.ScanAll(spolicy, namespace, namespace)
	// deal with the error here
	if err != nil {
		log.Println("not able to scan ", err)
	} else {
		for res := range recs.Results() {
			if res.Err != nil {

				log.Println("there was an error: ", err)
			} else {
				// process record here
				for bkey, value := range res.Record.Bins {
					bandMap[bkey] = value.(string)
				}
			}
		}
	}

	return bandMap, err

}
