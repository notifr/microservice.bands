package requestBuilder

import (
	"zhaw.ch/microservice.bands/persistenceUtil"
	"zhaw.ch/microservice.bands/stringUtility"
)

// BuildRequest for a given url. returns all the requests to execute in a string array
func BuildRequest() ([]string, error) {
	actors, err := getActors()
	events := make([]string, len(actors))
	for i := 0; i < len(actors); i++ {
		events[i] = stringUtility.GenerateString(actors[i])
	}
	return events, err
}

func getActors() ([]string, error) {
	data, err := persistenceUtil.GetData()
	return data, err
}
