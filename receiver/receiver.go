package receiver

import (
	"zhaw.ch/microservice.bands/pubnub"
)

// Receive messages from Provider, this is an adapter.
func Receive() error {
	return pubnub.SubscribeToPubnub()
}

// StopReceive messages
func StopReceive() {
	pubnub.StopSubscription()
}
